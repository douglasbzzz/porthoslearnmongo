<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 15:19
 */
$erro = isset($_GET['erro']) ? $_GET['erro'] : 3;

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Login</title>

</head>
<body>
<div class="container">
    <form class="card shadow p-3 mb-5 bg-white rounded formulario col-md-5 offset-md-2" id="frmPesquisa"
          action="loginController.php" method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <h3>Login</h3>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <input type="text" id="txtLogin" class="form-group" name="txtLogin" required placeholder="Login ou e-mail"/>
                <br/>
                <input type="password" id="txtPass" class="form-group" name="txtPass" required placeholder="Senha"/>
                <br/>
                <input type="submit" name="btnEnvia" class="btn btn-outline-primary" id="btnEnvia" value="Login"/>
                <br/>
                <?php
                if ($erro == 1) {
                    echo '<font color="#FF0000">Usuário ou Senha não encontrados, Verifique!<br/></font>';
                } elseif ($erro == 2) {
                    echo '<font color="#008000">Wellcome<br/></font>';
                } elseif ($erro == 4){
                    echo '<font color="#FF0000">Sua sessão expirou! Logue novamente, por favor.<br/></font>';
                }
                ?>
                <br/>
                <a href="index.php">Primeiro acesso? Crie seu usuário...</a>
            </div>
        </div>
    </form>
</div>
</body>
</html>

