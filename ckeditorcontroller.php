<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 09:26
 */
    session_start();
    date_default_timezone_set("America/Sao_Paulo");
    require_once ("class/Post.php");
    include "class/nFunctions.php";
    if(!authSession($_SESSION['datahoralogin'],date("Y-m-d H:i:s"),'%i')==1){
        header("location:index.php?erro=4");
    }else{
        $idUser = $_SESSION['idusuario'];

        if (!empty($_POST) && (empty($_POST['editor2']) || empty($_POST['editor2']))) {
            header("Location: index.php"); exit;
        }
        $oPost = new Post();
        $oPost->setIdUser($idUser);
        $oPost->setLongDesc($_POST['editor2']);
        $oPost->setShortDesc(strip_tags($_POST['editor2']));
        try{
            if($oPost->save()){
                echo "deu certo!";
                echo "<a href='index.php'>Voltar</a>";
            }
        }catch (Exception $e){
            echo "Houve um problema...".$e->getMessage();
            echo "<a href='index.php'>Voltar</a>";
        }
    }
?>