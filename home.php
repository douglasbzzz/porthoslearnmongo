<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 15:24
 */
    session_start();
    require_once ("class/nFunctions.php");
    date_default_timezone_set("America/Sao_Paulo");
    if(authSession($_SESSION['datahoralogin'],date("Y-m-d H:i:s"),"%i")==1){
        echo "Olá ".$_SESSION['userfirstname'].", seja bem vindo ao Learn2.0 <br/>";
    }else{
        echo "Sua sessão encerrou <br/>";
        header("location:login.php?erro=4");
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>home</title>

</head>
<body>
    <div class="container">
        <div class="form-group">
            <a href="ckeditor.php">CKEditor</a>
            <br/>
            <a href="recupera.php">Recuperar Posts Criados</a>
            <br/>
        </div>
    </div>
</body>
</html>
