<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 08:57
 */
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Incluir Usuário</title>
</head>
<body>
<form class="form-control" name="frmDados" id="frmDados" action="newUserController.php" method="post">
    <div class="form-group col col-3">
        <input type="text" id="txtNome" name="txtNome" placeholder="Nome" class="form-control"/>
        <input type="text" id="txtSobrenome" name="txtSobrenome" placeholder="Sobrenome" class="form-control"/>
        <input type="text" id="txtLogin" name="txtLogin" placeholder="Login" class="form-control"/>
        <input type="email" id="txtEmail" name="txtEmail" placeholder="E-Mail" class="form-control"/>
        <input type="password" id="txtSenha" name="txtSenha" placeholder="Senha" class="form-control"/>

        <input type="submit" value="Criar" id="btnEnviar" name="btnEnviar" class="btn btn-outline-success"/>


        <a href="login.php">Já tem usuário? faça login...</a>
    </div>

</form>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
