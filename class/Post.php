<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 09:28
 */

    class Post{
        private $idUser;
        private $longDesc;
        private $shortDesc;
        private $date;

        /**
         * @return mixed
         */
        public function getIdUser()
        {
            return $this->idUser;
        }

        /**
         * @param mixed $idUser
         */
        public function setIdUser($idUser)
        {
            $this->idUser = $idUser;
        }

        /**
         * @return mixed
         */
        public function getLongDesc()
        {
            return $this->longDesc;
        }

        /**
         * @param mixed $longDesc
         */
        public function setLongDesc($longDesc)
        {
            $this->longDesc = $longDesc;
        }

        /**
         * @return mixed
         */
        public function getShortDesc()
        {
            return $this->shortDesc;
        }

        /**
         * @param mixed $shortDesc
         */
        public function setShortDesc($shortDesc)
        {
            $this->shortDesc = $shortDesc;
        }

        /**
         * @return mixed
         */
        public function getDate()
        {
            return $this->date;
        }

        /**
         * @param mixed $date
         */
        public function setDate($date)
        {
            $this->date = $date;
        }

        public function save(){
            date_default_timezone_set("America/Sao_Paulo");
            //$manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
            $bulk = new MongoDB\Driver\BulkWrite();
            $bulk->insert([
                'descricaolonga'=>$this->longDesc,
                'descricaobreve'=>$this->shortDesc,
                'data'=>date("Y-m-d H:i:s"),
                'idusuario'=>$this->idUser
            ]);
            try{
                $manager = new \MongoDB\Driver\Manager("mongodb://localhost:27017");
                $write = new MongoDB\Driver\WriteConcern(MongoDb\Driver\WriteConcern::MAJORITY,100);
                if($result = $manager->executeBulkWrite('porthos.porthosArticles',$bulk,$write)){
                    return 1;
                }else{
                    return 0;
                }
            }catch (MongoException $e){
                echo "Houve um problema para gravar.".$e->getMessage();
            }
        }

    }
?>