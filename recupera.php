<?php
/**
 * Created by PhpStorm.
 * User: dougl
 * Date: 29/07/2018
 * Time: 10:24
 */
 require_once ("getRecordsMongo.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Recupera Artigo</title>
</head>
<body>
    <div class="container">
        <form class="form-control" name="frmRecupera" id="frmRecupera" method="post">
            <div class="form-group">
                <h3>Artigos disponíveis</h3>
                <?php
                    getRecord();
                ?>
            </div>
        </form>
    </div>
</body>
</html>
